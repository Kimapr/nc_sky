-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs, vector
    = minetest, nodecore, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local S = minetest.get_translator(modname)

local alldirs = nodecore.dirs()

local living = modname .. ":spore"

minetest.register_node(living, {
		description = S"Sponge Spore",
		tiles = {"nc_tree_humus.png^(nc_sponge.png^[opacity:64)^nc_tree_peat.png"},
		groups = {
			crumbly = 2,
		},
		sounds = nodecore.sounds("nc_terrain_swishy")
	})

nodecore.register_craft({
		label = "make sponge spore",
		action = "pummel",
		toolgroups = {thumpy = 2},
		nodes = {
			{
				match = {name = "nc_tree:peat", count = 8},
				replace = living
			}
		}
	})

nodecore.register_limited_abm({
		label = "Sponge Spore Sprouting",
		interval = 10,
		chance = 1,
		limited_max = 1000,
		nodenames = {living},
		action = function(pos, node)
      local vdirs = {}
      local opos = pos
      local moist,sand = 0,0
      local chance = 40
      for _,dir in pairs(alldirs) do
        local pos2 = vector.add(pos,dir)
        local name = minetest.get_node(pos2).name
        local s = minetest.get_item_group(name,"sand")
        local m = minetest.get_item_group(name,"moist")
        sand,moist = sand+s,moist+m
      end
      if moist > 0 and sand > 0 then
        chance = chance / math.sqrt(moist*sand)
        if math.random(math.floor(chance+.5)) == 1 then
          minetest.set_node(pos,{name="nc_sponge:sponge_living"})
        end
      end
    end
  })
