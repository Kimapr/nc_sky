-- LUALOCALS < ---------------------------------------------------------
local nodecore, minetest
    = nodecore, minetest
-- LUALOCALS > ---------------------------------------------------------

local S = minetest.get_translator("nc_crystal")

--[[local addhint = nodecore.addhint]]
local modname = minetest.get_current_modname()

local stone_nodes = {}

local tex = "(nc_lode_ore.png^nc_crystal_living.png)^[mask:nc_lode_mask_prill.png"
local tex_ore = "nc_terrain_stone.png^((nc_lode_ore.png^nc_crystal_living.png)^[mask:nc_lode_mask_ore.png)"
local crystal = modname .. ":crystal"
local ore = modname..":ore"

--[[addhint(S"find lode crystal",
	ore
)

addhint(S"plant lode crystal",
	"lode crystal planting",
  ore
)]]

minetest.register_craftitem(crystal, {
		description = "~DEPRECATED~",
		inventory_image = tex,
		wield_image = tex,
		sounds = nodecore.sounds("nc_lode_annealed"),
		on_place = function(itemstack, placer, pointed_thing, ...)
			if not nodecore.interact(placer) then return end
			if itemstack:get_name() == crystal and pointed_thing.type == "node" then
				local pos = pointed_thing.under
				for _, v in ipairs(stone_nodes) do
					if nodecore.match(pos, {
							name = v,
						}) then
						minetest.set_node(pos,{name=ore})
						minetest.sound_play("nc_lode_annealed",{pos=pos})
						itemstack:set_count(itemstack:get_count() - 1)
						if placer then
							nodecore.player_stat_add(1, placer, "craft", "lode crystal planting")
						end
						return itemstack
					end
				end
			end
      return minetest.item_place(itemstack, placer, pointed_thing, ...)
		end
	})

minetest.register_node(ore, {
    description = "~DEPRECATED~",
    tiles = {tex_ore},
    is_ground_content = true,
    groups = {cracky = 2, lodey = 1},
    sounds = nodecore.sounds("nc_terrain_stony"),
		drop_in_place = "nc_terrain:stone",
    drop = crystal
  })

local growdirs = {
  {x= 1,y=0,z=0},
  {x=0,y= 1,z=0},
  {x=0,y=0,z= 1},
  {x=-1,y=0,z=0},
  {x=0,y=-1,z=0},
  {x=0,y=0,z=-1},
}

nodecore.register_limited_abm({
  label = "Lode Crystal Deprecation",
  nodenames = {ore},
  interval = 1,
  chance = 1,
  action = function(pos,node)
    minetest.set_node(pos,{name="nc_lode:ore"})
  end
})

nodecore.register_limited_abm({
    label = "Lode Crystal growing",
    nodenames = {ore},
    neighbours = {"group:lava"},
    interval = 60,
    chance = 30,
    action = function(pos,node)
      local vdirs = {}
      for _,off in pairs(growdirs) do
        local pos = vector.add(pos,off)
        if minetest.get_item_group(minetest.get_node(pos).name,"stone") > 0 then
          table.insert(vdirs,pos)
        end
      end
      if #vdirs > 0 then
        local pos = vdirs[math.random(1,#vdirs)]
        if minetest.find_node_near(pos,1,{"group:lava"}) then
          minetest.set_node(pos,{name=ore})
          minetest.sound_play("nc_lode_annealed",{pos=pos})
        end
      end
    end
  })


nodecore.register_craft({
		label = "kill poor crystal",
		action = "pummel",
		nodes = {
			{match = ore, replace = "nc_lode:ore"}
		},
		toolgroups = {thumpy = 2}
	})

minetest.after(0.001,function()
  
  for name,def in pairs(minetest.registered_nodes) do
    
    if minetest.get_item_group(name,"stone") > 0 then
      table.insert(stone_nodes,name)
    end
    
  end

end)
