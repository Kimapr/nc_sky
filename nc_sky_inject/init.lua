-- LUALOCALS < ---------------------------------------------------------
local nodecore
    = nodecore
-- LUALOCALS > ---------------------------------------------------------

local ntgr = nodecore.tree_growth_rate

function nodecore.tree_growth_rate(pos)
  return ntgr(pos)*10
end

nodecore.register_craft({
		label = "melt stone to lava",
		action = "cook",
		touchgroups = {flame = 4},
		duration = 30,
		cookfx = true,
		nodes = {
			{
				match = "nc_terrain:stone",
				replace = "nc_terrain:lava_source"
			}
		}
	})

nodecore.register_cook_abm({nodenames = {"nc_terrain:stone"}, neighbors = {"group:flame"}})

assert(minetest.registered_nodes["nc_concrete:aggregate_wet_source"])

nodecore.register_limited_abm({
--minetest.register_abm({
  label = "aggregate insta-cooking",
  interval = 1,
  chance = 1,
  nodenames = {"nc_concrete:aggregate_wet_source"},
  neighbors = {"group:lava"},
  action = function(pos,node)
    print("aggregate abm running")
    local nnode
    if math.random() > (1/10) then
      nnode = {name = "nc_terrain:hard_stone_1"}
    else
      local t = {"nc_lux:stone","nc_terrain:hard_stone_2"}
      nnode = {name=t[math.random(1,#t)]}
    end
    minetest.set_node(pos,nnode)
    nodecore.node_sound(pos,"place")
  end
})

nodecore.register_craft({
    label = "break amalgamation to lode prills",
    action = "pummel",
    nodes = {
      {match = "nc_igneous:amalgam_loose", replace = "nc_terrain:gravel"},
    },
    items = {
      {name = "nc_lode:prill_hot", count = 4, scatter = 5, velocity = {x=0,y=5,z=0}}
    },
    toolgroups = {cracky = 2},
    itemscatter = 5
  })

nodecore.register_craft({
    label = "break harder amalgamation to lode prills",
    action = "pummel",
    nodes = {
      {match = "nc_igneous:amalgam", replace = "nc_terrain:gravel"},
    },
    items = {
      {name = "nc_lode:prill_hot", count = 4, scatter = 5, velocity = {x=0,y=5,z=0}}
    },
    toolgroups = {cracky = 4},
    itemscatter = 5
  })
